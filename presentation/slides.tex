\documentclass[aspectratio=169,t]{beamer}
%
% Choose how your presentation looks.
%
% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
%
\mode<presentation>
{
  \usetheme{default}      % or try Darmstadt, Madrid, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{default}  % or try serif, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
  \setbeamertemplate{footline}[frame number]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{lmodern}% http://ctan.org/pkg/lm
\usepackage{subcaption}
\captionsetup{justification=centering}
\usepackage{color, colortbl}

% draw nice pictures
\usepackage{tikz}
\usetikzlibrary{backgrounds,shapes,arrows,positioning,calc,snakes,fit}
\usepgflibrary{decorations.markings}

\title{Efficient SAT-based Boolean Matching for FPGA~Technology~Mapping}
\author{Nikitha, Pascal, Tim}
\date{\today}

\begin{document}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}{FPGA Introduction}% 10 - 15 minutes

\begin{block}{Field Programmable Gate Array (FPGA)}
\begin{itemize}
  \item Integrated circuit
  \item Arrays of Configurable Logic Blocks (CLB)
  \item Reconfigurable interconnects
\end{itemize}
\end{block}
\begin{figure}
\centering
\begin{subfigure}[b]{0.4\textwidth}
  \includegraphics[height=0.4\textheight]{altera.jpg}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
  \includegraphics[height=0.4\textheight]{xilinx.jpg}
\end{subfigure}
\caption{Different FPGA chips\\
\tiny\url{https://en.wikipedia.org/wiki/Field-programmable_gate_array}}
\end{figure}

\end{frame}


\begin{frame}{FPGA Applications}

\begin{columns}[T]

    \begin{column}{0.48\textwidth}
\begin{block}{Automotive}
\begin{itemize}
\item High reliability
\item Low power
\end{itemize}
\end{block}

\begin{block}{Communications}
\begin{itemize}
\item Minimal energy consumption
\item Small physical footprint
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.48\textwidth}
\begin{block}{Defense}
\begin{itemize}
\item Anti-tamper
\item Supply chain security
\end{itemize}
\end{block}

\begin{block}{Space}
\begin{itemize}
\item SEU immune configuration
% single event upset, ionizing particles cause transient bit switches
\item RT/High-speed processing
\end{itemize}
\end{block}
    \end{column}

\end{columns}

\begin{figure}
\includegraphics[width=0.6\textwidth]{technologies_overview.png}
\caption{Comparison of different technologies\\
\tiny\url{https://docs.microsoft.com/en-us/azure/machine-learning/service/concept-accelerate-with-fpgas}}
\end{figure}

\end{frame}


\begin{frame}{FPGA Internal Structure - 1}

\begin{columns}[T]

    \begin{column}{0.48\textwidth}
\begin{block}{I: Configurable Logic Blocks}
\begin{itemize}
\item Basic computation/storage elements
\item Combinational logic, flip-flop and full adder
\end{itemize}
\end{block}

\begin{block}{II: Programmable Routing}
\begin{itemize}
\item Connection between logic and I/O blocks
% pass transistor: not directly connected to supply voltage, pass signal on
% tri-state buffer: high impedance state on selected outputs, enabling multiple
%   inputs to use the same bus system
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.48\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{architecture.png}
\caption{FPGA architecture overview\\\tiny\url{https://allaboutfpga.com/fpga-architecture/}}
\end{figure}
    \end{column}

\end{columns}

\end{frame}


\begin{frame}{FPGA Internal Structure - 2}

\begin{columns}[T]

    \begin{column}{0.48\textwidth}
\begin{block}{III: Programmable I/O}
\begin{itemize}
\item Interface to external components
\item Huge area, complex design
\end{itemize}
\end{block}

\begin{block}{Other}
\begin{itemize}
\item ALUs, block RAM, DSPs, \dots
\item Specialized circuits for often needed tasks
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.48\textwidth}
\setcounter{figure}{2}
\begin{figure}
\includegraphics[width=\textwidth]{architecture.png}
\caption{FPGA architecture overview\\\tiny\url{https://allaboutfpga.com/fpga-architecture/}}
\end{figure}
    \end{column}

\end{columns}

\end{frame}


\begin{frame}{Configurable Logic Blocks}

\begin{columns}[T]

    \begin{column}{0.38\textwidth}
\begin{block}{Components}
\begin{itemize}
\item 4-input Lookup Table
\item D-type flip-flop
\item Full adder
\end{itemize}
\end{block}

\begin{block}{Behaviour}
\begin{itemize}
\item Normal/arithmetic mode
\item Synch/asynch output
\item Provide reconfigurable logic gates
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.58\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{clb.png}
\caption{Configurable Logic Block\\\tiny\url{https://en.wikipedia.org/wiki/Logic_block}}
\end{figure}
    \end{column}

\end{columns}

\end{frame}


\begin{frame}{Lookup Table}

\begin{columns}[T]

    \begin{column}{0.48\textwidth}
\begin{block}{Multiplexer}
\begin{itemize}
\item Simple implementation of LUT
% flip flop: stores one bit of data
% latch: stores any number of bits using a single trigger
\item Select lines driven by gated D latch (reconfigurable)
\item n-bit LUT encodes any n-input boolean function
\item Size of LUTs is limited
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.48\textwidth}
\begin{figure}
    \begin{subfigure}[h]{0.4\textwidth}
\includegraphics[width=\textwidth]{mux.pdf}
    \end{subfigure}
    \begin{subfigure}[h]{0.4\textwidth}
\begin{centering}
\begin{tabular}{| c | c |}\hline
\rowcolor{gray!40} \textit{$S_0$} & \textit{$Z$} \\ \hline
\rowcolor{gray!10} 0 & $\overline{\text{A}}$ \\ \hline
\rowcolor{gray!10} 1 & A \\ \hline
\end{tabular}
\end{centering}
    \end{subfigure}
\caption{Multiplexer and truth table\\\tiny\url{https://en.wikipedia.org/wiki/Multiplexer}}
\end{figure}

    \end{column}

\end{columns}

\end{frame}


\begin{frame}{Technology Mapping}

\begin{columns}[T]

    \begin{column}{0.48\textwidth}
\begin{block}{The Problem:}
Mapping of logic expressions to new logic representation that can be implemented
by FPGA resources.
\end{block}

\begin{block}{Details:}
\begin{itemize}
\item Fixed number of LUT inputs
\item Implement logic functions with $vars >> inputs_{\text{\ LUT}}$
\item Minimize used area/latency
\end{itemize}
\end{block}
    \end{column}

    \begin{column}{0.48\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{covering.png}
\caption{Lookup table configuration}
\end{figure}
    \end{column}

\end{columns}

\end{frame}


%\begin{frame}{Technology Mapping Approaches for FPGAs}
%
%\begin{block}{Structural matching}
%\begin{itemize}
%  \item Functional decomposition and pattern matching to map pattern graphs onto
%subject graph
%  \item Overall performance is worsened by heuristics (decomposition and
%partitioning)
%%decomposition: reduce size of subject graph to matching pattern graph and
%%exploit recurring expressions
%  \item Tree based approaches ignore boolean equalities
%\end{itemize}
%\end{block}
%
%\begin{block}{Boolean matching}
%\begin{itemize}
%  \item Canonicity and Boolean Signatures to exploit boolean equalities
%  \item Consideres permutation of input variables, negation of input variables
%and negation of output variables (only applicable if input/output can be
%inverted)
%\end{itemize}
%\end{block}
%
%\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{The Problem - Formal Definition}
Given a network of interconnected CLBs, then let be:
\begin{itemize}
 \item $H(P)$ the \texttt{configuration} (interconnected CLBs)
 \item $P = \{p_1, p_2, ..., p_m\}$ the set of input pins of the configuration with $|P| = m $
 \item $f(X)$ the input function
 \item $X = \{x_1, x_2, ..., x_n\}$ the set of inputs of the function with $|X|= n$
\end{itemize}

\begin{block}{The Problem:}
Boolean matching is the process of determining whether a configuration $ H(P)$ can implement a function
$f(X)$ for $|X| = n \leq m =|P|$.
\end{block}
\end{frame}


\begin{frame}{Prerequisites - Converting Primitive Gates to CNF}

Transform primitive gates to CNF in linear time:

\begin{center}
\begin{tikzpicture}[scale=1]
    %left 2-input and
    \draw (-2.5,3.3) rectangle (-1.5,1.7);
    \draw (-3, 3) -- (-2.5,3);
    \draw (-3, 2) -- (-2.5,2);
    \draw (-1.5,2.5) -- (-1,2.5);
    \node at (-2, 2.5) {\&};
    \node at (-3.25, 3) {$x_1$};
    \node at (-3.25, 2) {$x_2$};
    \node at (-0.8, 2.5) {y};

    %line in the middle
    \draw [dashed] (0.78, 3.5) -- (0.78,-1.25);

    %right 3-input and
    \draw (4.5,3.3) rectangle (5.5,1.7);
    \draw (4, 3.1) -- (4.5,3.1);
    \draw (4, 2.5) -- (4.5,2.5);
    \draw (4, 1.9) -- (4.5,1.9);
    \draw (5.5,2.5) -- (6,2.5);
    \node at (5, 2.5) {\&};
    \node at (3.75, 3.1) {$x_1$};
    \node at (3.75, 2.5) {$x_2$};
    \node at (3.75, 1.9) {$x_3$};
    \node at (6.2, 2.5) {y};

    \node[draw,text width=5.5cm] at (-2.5,0) {\tiny\vspace{-0.5cm}
        \begin{align*}
            y \leftrightarrow (x_1 \land x_2) & \equiv (y \rightarrow (x_1 \land x_2)) \land ((x_1 \land x_2) \rightarrow y)\\
            & \equiv (\overline{y} \lor (x_1 \land x_2)) \land (\overline{(x_1 \land x_2)} \lor y)\\
            & \equiv (\overline{y} \lor (x_1 \land x_2)) \land (\overline{x_1} \lor \overline{x_2} \lor y)\\
            & \equiv (\overline{y} \lor x_1) \land (\overline{y} \lor x_2) \land (\overline{x_1} \lor \overline{x_2} \lor y)\\
            & \equiv \langle [\overline{y}, x_1] ,[\overline{y}, x_2] , [\overline{x_1}, \overline{x_2} , y]\rangle
        \end{align*}};

    \node[draw,text width=7.375cm] at (5,0) {\tiny\vspace{-0.5cm}
        \begin{align*}
            y \leftrightarrow (x_1 \land x_2 \land x_3) & \equiv (y \rightarrow (x_1 \land x_2 \land x_3)) \land ((x_1 \land x_2 \land x_3) \rightarrow y)\\
            & \equiv (\overline{y} \lor (x_1 \land x_2 \land x_3)) \land (\overline{(x_1 \land x_2 \land x_3)} \lor y)\\
            & \equiv (\overline{y} \lor (x_1 \land x_2) \land x_3) \land (\overline{x_1} \lor \overline{x_2} \lor \overline{x_3} \lor y)\\
            & \equiv (\overline{y} \lor x_1) \land (\overline{y} \lor x_2) \land (\overline{y} \lor x_3) \land (\overline{x_1} \lor \overline{x_2} \lor \overline{x_3} \lor y)\\
            & \equiv \langle [\overline{y}, x_1],[\overline{y}, x_2],[\overline{y}, x_3], [\overline{x_1}, \overline{x_2}, x_3, y]\rangle
        \end{align*}
    };
  
\end{tikzpicture}
\end{center}

Transformation of $n$-input AND will result in $n+1$ clauses\footnote{\tiny T.
Larrabee. Test pattern generation using Boolean satisfiability. IEEE Trans. on
CAD, 11:4–15, 1992}
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Prerequisites - Converting MUX to CNF}

Transform 2-input multiplexer to CNF using NAND gates:

\begin{center}

\begin{tikzpicture}[scale=1]

    %---------------------------------- Multiplexer ----------------------------
    \coordinate (A) at (-8, 3);
    \draw ($(A)$) -- ($(A) + (0, -3)$);
    \draw ($(A) + (1.5, -0.8)$) -- ($(A) + (1.5, -2.2)$);
    \draw ($(A)$) -- ($(A) + (1.5, -0.8)$);
    \draw ($(A) + (0, -3)$) -- ($(A) + (1.5, -2.2)$);
    \node at ($(A) + (0.75, -1.5)$) {MUX};
    \draw ($(A) + (1.5, -1.5)$) -- ($(A) + (2, -1.5)$); %output
    \node at ($(A) + (2.2, -1.5)$) {$y$};
    \draw ($(A) + (-0.5, -0.75)$) -- ($(A) + (0, -0.75)$); %first input
    \node at ($(A) + (0.2, -0.75)$) {$0$};
    \node at ($(A) + (-0.75, -0.75)$) {$x_1$};
    \draw ($(A) + (-0.5, -2.25)$) -- ($(A) + (0, -2.25)$); %second input
    \node at ($(A) + (0.2, -2.25)$) {$1$};
    \node at ($(A) + (-0.75, -2.25)$) {$x_2$};
    \draw ($(A) + (0.75, 0)$) -- ($(A) + (0.75, -0.4)$); %select line
    \node at ($(A) + (0.75, +0.15)$) {$s$};
    
    
    %---------------------------------- Arrow -----------------------------------
    \draw (-5.25, 1.375) -- (-4.25, 1.375);
    \draw (-5.25, 1.625) -- (-4.25, 1.625);
    \draw (-4.45, 1.2) -- (-3.95, 1.5);
    \draw (-4.45, 1.8) -- (-3.95, 1.5);
    
    %----------------------------------- primitive Logic Gates ----------------
    %top left 2-input NAND
    \draw (-2.5,3.3) rectangle (-1.5,1.7);
    \draw (-3, 3) -- (-2.5, 3); %i_top
    \node at (-3.25, 3) {$x_1$};
    \draw (-3, 2) -- (-2.5, 2); % input bot
    \node at (-3.25, 2) {$s$};
    \draw (-1.5,2.5) -- (-1, 2.5); %output
    \node at (-2, 2.5) {\&};
    \draw [fill=black](-1.4, 2.5) circle [radius=0.1cm];
    
    %bot left 2-input NAND
    \draw (-2.5,1.3) rectangle (-1.5,-.3);
    \draw (-3, 1) -- (-2.5, 1); %input top
    \node at (-3.25, 1) {$\overline{s}$};
    \draw (-3, 0) -- (-2.5, 0); %input bot
    \node at (-3.25, 0) {$x_2$};
    \draw (-1.5,0.5) -- (-1, 0.5); %output
    \node at (-2, 0.5) {\&};
    \draw [fill=black](-1.4, 0.5) circle [radius=0.1cm];
    
    %right 2-input NAND
    \draw (-0.5, 2.3) rectangle (0.5, 0.7);
    \draw (-1, 2) -- (-0.5, 2); %input top 
    \draw (-1, 1) -- (-0.5, 1); %input bot
    \draw (0.5, 1.5) -- (1, 1.5); %output
    \node at (1.25, 1.5) {$y$};
    \node at (0, 1.5) {\&};
    \draw [fill=black](0.6, 1.5) circle [radius=0.1cm];
    
    %connect the NANDS
    \draw (-1, 2.5) -- (-1, 2);
    \draw (-1, 0.5) -- (-1, 1);    
\end{tikzpicture}
\end{center}

Resulting CNF:

\[
    \langle [ \overline{x_1}, \overline{x_2}, y], [x_1, x_2, \overline{y}], [s, x_2, y], 
    [s, \overline{x_2}, y], [\overline{s}, x_1, y], [\overline{s}, \overline{x_1}, \overline{y}]\rangle
\]

\end{frame}


\begin{frame}{Standard Approach - 1}
Function with $n$ input variables and configuration with $m$ input pins:
\begin{itemize}
 \item All possible function input $\leftrightarrow$ configuration pin mappings must be considered
 \item If $n = m$ there are $n!$ possible mappings $\Rightarrow n!$ clauses
 \item Pin $\leftrightarrow$ input assingment represented by multiplexers to
avoid this
% \item One multiplexer for each input pin of the configuration
% \item Select line of the multiplexers represent the mapping
\end{itemize}

\ \\

CNF of the multiplexer combined with the configuration is denoted as:
    \[
        CNF(MUX)\land CNF(H)
    \]
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Standard Approach - 2}
\begin{itemize}
% \item Implement each of the $2^{|X|}$ input combinations
 \item Replicating the formula $2^{n}$ times for the different inputs
($\overline{\text{A}}$ and A)
 \item Each replication is referred to as a stamp $d$
% \item $d$ is the decimal number of the input
 \item $BIN(d)$ is the binary encoding of $d$
% \item $MUX(X)_d$ and $H_d$ belong to the the stamp $d$
\end{itemize}

\ \\

Each indivial formula for each stamp looks like the following:
    \[
  CNF(H_d) \land CNF(MUX(X)_d) \land BIN(d) \land f(d)
    \]


Where $f(d)$ is the result of the function $f$.
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Standard Approach - 3}
Taking the $2^{n}$ different inputs and thus different stamps into account
results in the following formula:

    \[
  \phi = \bigwedge_{\substack{d=0}}^{2^{n}-1} CNF(H_d) \land CNF(MUX(X)_d) \land BIN(d) \land f(d)
    \]


\begin{itemize}
 \item Reduces complexity by a lot
 \item Improvements still possible
\end{itemize}

\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Proposed Approach - Prerequisites}

$n$-LUT can implement every function with $n$ function inputs

\begin{center}
\begin{tikzpicture}[scale=1]
    \coordinate (A) at (-2.5, 4);
    %left 2-input and
    \draw (A) rectangle ($(A) + (2, -3)$);
    %input lines
    \draw ($(A) + (-0.5, -0.5)$) -- ($(A) + (0, -0.5)$);
    \draw ($(A) + (0.5, 0)$) -- ($(A) + (0.5, -3)$);
    \draw ($(A) + (-0.5, -1.25)$) -- ($(A) + (0, -1.25)$);
    \draw ($(A) + (-0.5, -2)$) -- ($(A) + (0, -2)$);
    \draw ($(A) + (-0.5, -2.75)$) -- ($(A) + (0, -2.75)$);
    \node at ($(A) + (0.25, -0.5)$)  {$0$};
    \node at ($(A) + (0.25, -1.25)$) {$1$};
    \node at ($(A) + (0.25, -2)$)    {$2$};
    \node at ($(A) + (0.25, -2.75)$) {$3$};
    %output line
    \draw ($(A) + (2, -1.5)$) -- ($(A) + (2.5, -1.5)$);
    \node at ($(A) + (1.25, -1.5)$) {2-LUT};
    \node at ($(A) + (-.75, -0.5)$)  {$0$};
    \node at ($(A) + (-.75, -1.25)$) {$1$};
    \node at ($(A) + (-.75, -2)$)    {$0$};
    \node at ($(A) + (-.75, -2.75)$) {$0$};
    \node at ($(A) + (2.7, -1.5)$) {y};
    %select lines
    \draw ($(A) + (1.05, -3.5)$) -- ($(A) + (1.05, -3)$);
    \draw ($(A) + (1.55, -3.5)$) -- ($(A) + (1.55, -3)$);
    \node at ($(A) + (1.05, -3.75)$) {$x_1$};
    \node at ($(A) + (1.55, -3.75)$) {$x_2$};
    
    \node at ($(A) + (1.05, -2.75)$) {$i_1$};
    \node at ($(A) + (1.55, -2.75)$) {$i_0$};
    %text
    \node[text width = 6cm] at (4, 2.2) {
        Example: $f(x_1, x_2) = \overline{x_1} \land x_2$\\
        \begin{itemize}
         \item 2-LUT can implement every function with 2 inputs
         \item Select lines $i_1, i_0$ choose entry of the LUT
        \end{itemize}
    };
\end{tikzpicture}
\end{center}


$\Rightarrow$ Partitioning the functions into groups with function inputs
corresponding to the input size of $n$-LUTs guarantees the exsistence of a
solution
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Proposed Approach - Two Stages}
New approach based on the observations before:
\begin{enumerate}
 \item Coarse pin assignment
 \begin{itemize}
    \item Replace and simplify all CLBs of the target configuration with $k$-LUTs
    \item Partition functions into groups containing $k$ inputs
    \item These $k$-LUTs are used for creating the $2^{n}$ stamps
    \item Much easier problem as the partitions are independent of the mapping
 \end{itemize}
 \item Detailed pin assigment
 \begin{itemize}
    \item Performing a fine grain mapping for the given sets of partitions
    \item Adding partition restriction encoded as CNF to formula
    \item Allows much less permutations
 \end{itemize}
\end{enumerate}
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Proposed Approach - Advantages}
\begin{enumerate}
 \item Number of permutations:
 \begin{itemize}
    \item For example forming $l$ equally sized partitions result in a reduced complexety
    of $O(\frac{n}{l}!)$ instead of $O(n!)$. 
 \end{itemize}
 \ \\
 \item Fast over-approximation:
 \begin{itemize}
    \item UNSAT outcome in the fast first step signals that the function can not
be implemented by the configuration $H$ 
 \end{itemize}
\end{enumerate}

\end{frame}


%---------------------------------------------------------------------------------------
%
%\newcommand{\FiveLUT}[2]{
%    %top left point of the rectangle from which the coordinates are calculated relative
%    \coordinate (A) at (#1, #2);
%    %left 2-input and
%    \draw (A) rectangle ($(A) + (2, -3)$);
%    %input lines
%    \draw ($(A) + (-0.5, -0.5)$) -- ($(A) + (0, -0.5)$);
%    \draw ($(A) + (-0.5, -1.)$) -- ($(A) + (0, -1.)$);
%    \draw ($(A) + (-0.5, -1.5)$) -- ($(A) + (0, -1.5)$);
%    \draw ($(A) + (-0.5, -2)$) -- ($(A) + (0, -2)$);
%    \draw ($(A) + (-0.5, -2.5)$) -- ($(A) + (0, -2.5)$);
%    %output line
%    \draw ($(A) + (2, -1.5)$) -- ($(A) + (2.5, -1.5)$);
%    \node at ($(A) + (1, -1.5)$) {5-LUT};
%}

%---------------------------------------------------------------------------------------

\begin{frame}{Search Space Reduction}
\begin{itemize}
    \item Performance of a SAT Solver can be further improved by reducing its search space.
    \item This can be done using the following methods:
    \begin{enumerate}
        \item Reusing Conflict Clauses
        \item Constraining the Search Space
    \end{enumerate}
\end{itemize}

\end{frame}

%--------------------------------------------------------------------------------------

\begin{frame}{Search Space Reduction - Reusing Conflict Clauses}

\begin{block}{SAT Solver in general}
\begin{itemize}
    \item Finding a satisfiable model is enough
    \item SAT solvers learn from conflicts due to unsatisfiable variable assignments and generate conflict clauses
    \item Conflict clauses are used to prevent re-exploration of non-solution space
\end{itemize}
\end{block}

\begin{block}{Boolean matching technology mapping}
\begin{itemize}
    \item Optimal solution wanted (in terms of area, latency, \dots)
    \item Conflict clauses that are discovered can be shared across problem instances for configuration $H$
\end{itemize}
\end{block}

\end{frame}


\begin{frame}{Search Space Reduction - Reusing Conflict Clauses}

\begin{block}{Theorem:}
    CNF formulae $\Phi_{1}$ and $\Phi_{2}$ with $\forall c \in \{ \Phi_{1} -
    \Phi_{2}, \Phi_{2} - \Phi_{1}\}$ and $|c| = 1$
    \begin{itemize}
        \item $\exists c_{i} = \overline{c}$ where $c \in \Phi_{1}$ and $
            c_{i} \in \Phi_{2}$
        \item[OR]
        \item $\exists c_{i} = \overline{c}$ where $c \in \Phi_{2}$ and $c_{i} \in
            \Phi_{1}$.
    \end{itemize}

    Further assume that CNF $\Phi_{c}$ represents the conflict clauses
    deduced from $\Phi_{1}$. The CNF $\{\Phi_{2} \cup \Phi_{c}\}$ is
    satisfiable if and only if the CNF $\Phi_{2}$ is satisfiable.
\end{block}

\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Search Space Reduction - Constraining the Search Space}

\begin{itemize}
    \item The Constraints are encoded in CNF
    \item They reduce the complexity of the problem by limiting the assignment possibilities of the MUX select lines.
    \item Examples of some constraints:
    \begin{enumerate}
        \item Each function input should be mapped to a configuration pin.
        \item Do not map the same function input to different pins of the same $k$-LUT
    \end{enumerate}
\end{itemize}

\end{frame}


\begin{frame}{Results}

\begin{columns}[T]
    \begin{column}{0.48\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{results.png}
\caption{Run time comparison\footnotemark[2]}
\end{figure}
    \end{column}

    \begin{column}{0.48\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{example.png}
\caption{Chosen hardware configuration\footnotemark[2]}
\end{figure}
    \end{column}
\end{columns}
\footnotetext[2]{\tiny SAFARPOUR, Sean, et al. Efficient SAT-based Boolean
matching for FPGA technology mapping. In: 2006 43rd ACM/IEEE Design Automation
Conference. IEEE, 2006. S. 466-471}
\end{frame}


\begin{frame}{Questions?}

\end{frame}


\begin{frame}{Example}
\addtocounter{framenumber}{-1}
\begin{center}
\begin{itemize}
 \item Given: An architectur in which a CLB consists of 2 5-LUT and one MUX
 \item Goal: Implement a 11 input function
\end{itemize}
\begin{figure}
\includegraphics[height=4.5cm]{example.png}
\caption{Coarse pin assignment\footnotemark[3]}
\end{figure}
\end{center}
\footnotetext[3]{\tiny SAFARPOUR, Sean, et al. Efficient SAT-based Boolean
matching for FPGA technology mapping. In: 2006 43rd ACM/IEEE Design Automation
Conference. IEEE, 2006. S. 466-471}
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Example - Second step}
\addtocounter{framenumber}{-1}
\begin{center}
\begin{itemize}
 \item Transform CLB logic to 7-LUT
 \item Assume the following partitions yield SAT:
 \begin{itemize}
    \item[$\text{Partition}_\text{A}$] $= \{x_1 , x_6 , x_9, x_2 , x_4 , x_3 , x_8\}$
    \item[$\text{Partition}_\text{B}$] $= \{x_3 , x_5 , x_{10} , x_9 , x_7 , x_{11}\}$
 \end{itemize}
\end{itemize}

\begin{figure}
\includegraphics[height=3.75cm]{example2.png}
\caption{Detailed pin assignment\footnotemark[4]}
\end{figure}
\end{center}
\footnotetext[4]{\tiny SAFARPOUR, Sean, et al. Efficient SAT-based Boolean
matching for FPGA technology mapping. In: 2006 43rd ACM/IEEE Design Automation
Conference. IEEE, 2006. S. 466-471}
\end{frame}

%---------------------------------------------------------------------------------------

\begin{frame}{Example - Second step}
\addtocounter{framenumber}{-1}

\begin{itemize}
 \item Assume the second step terminates with SAT
 \item The solver return a solution:
\end{itemize}

\begin{center}
 \begin{tabular}{ |l|l|l|l| }
  \hline
  \multicolumn{2}{|c|}{Partition$_\text{A}$} & \multicolumn{2}{c|}{Partition$_\text{B}$}\\ \hline
  $x_2$ & CLB A  pin 1 & $x_3$& CLB B pin 1\\
  $x_9$ & CLB A  pin 2 & $x_{10}$& CLB B pin 2\\
  $x_8$ & CLB A  pin 3 & $x_5$& CLB B pin 3\\
  $x_6$ & CLB A  pin 4 & Output pin partition A & CLB B pin 4\\
  $x_4$ & CLB A  pin 5 & $x_7$& CLB B pin 5\\
  $x_1$ & CLB A  pin 6 & $x_{11}$& CLB B pin 6\\
  $x_3$ & CLB A  pin 7 & $x_{9}$& CLB B pin 7\\ \hline
\end{tabular}
\end{center}

\end{frame}


\begin{frame}{Search Space Reduction - Reusing Conflict Clauses}
\addtocounter{framenumber}{-1}
    \begin{block}{Proof:}
        We need to prove two cases
        \begin{enumerate}
            \item If $\{\Phi_{2} \cup \Phi_{c}\}$ is \textbf{UNSAT} then
                $\Phi_{2}$ is \textbf{UNSAT}
            \item If $\{\Phi_{2} \cup \Phi_{c}\}$ is \textbf{SAT} then
                $\Phi_{2}$ is \textbf{SAT}
        \end{enumerate}
    \end{block}
\begin{itemize}
\item For both the cases, it is sufficient to show that $ \Phi_{c} $ evaluates to true in the CNF $ \{ \Phi_{2} \cup \Phi_{c} \} $ .
\item  Any conflict clause $ c_{c} \in \Phi_{c} $ that contains a variable v which is also a clause $ c_{v}$ such that $ |c_{v}| = 1 $ and $ c_{v} \in \Phi_{1} $ is satisfiable in $ \Phi_{2} $.
\item Assuming WLOG that the literal for variable $ \overline{l} $ must appear in $ c_{v} \in \Phi_{1} $.
\item Since, $ c_{v} \in \{ \Phi_{1} - \Phi_{2} \} $, then there exists a clause $ c_{w} \in \{ \Phi_{2} - \Phi_{1} \} $ such that $ c_{w} = \overline{c_{v}} $.
\item As a result, $ \overline{\overline{l}} = l $ must be satisfied in $ \Phi_{2} $ which will also satisfy the conflict clause $ \Phi_{c} $.
\end{itemize}

\end{frame}


\end{document}
